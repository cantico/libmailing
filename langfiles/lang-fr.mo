��          �            h     i     u     }  i   �      �  $        <     D     G     _     d  5   {     �     �  g  �     :     J     R  �   a  3     :   ?     z     �  )   �     �     �  4   �  0        B                                                            
   	                 (Join file) API key API secret key If your server is not accessible threw internet you have to say yes. If it is we encourage you to say no. Internet seems to be accessible. Internet seems to not be accessible. Mailjet No Put image into the mail Save The email is mandatory The list of subscriptions must be an array of list ID This is a wrong email format Yes Project-Id-Version: LibMailing
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-02-20 12:08+0100
PO-Revision-Date: 2018-02-20 12:08+0100
Last-Translator: Antoine GALLET <antoine.gallet@cantico.fr>
Language-Team: Cantico <support@cantico.fr>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: iso-8859-1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: LibMailing_translate;LibMailing_translate:1,2
X-Generator: Poedit 1.8.7
X-Poedit-SearchPath-0: programs
X-Poedit-SearchPathExcluded-0: programs/api
 (Fichier joint) API key API secret key Si votre serveur n'est pas accessible depuis internet, l'option doit être mise à oui. Si il est accessible depuis internet il est conseiller de mettre l'option à non. Internet semble être accessible sur votre serveur. Internet semble ne pas être accessible sur votre serveur. Mailjet Non Intégrer les images dans le mail envoyé Enregistrer L'adresse email est obligatoire la liste des inscriptions doit être un tableau d'ID L'adresse email n'est pas formatée correctement Oui 