; <?php/*

[general]
name							="LibMailing"
version							="2.0.0"
encoding						="UTF-8"
description						="Mailing sending with webservices"
description.fr					="Gestion de mailing en utilisant des services web"
delete							=1
ov_version						="7.7.0"
php_version						="5.4.0"
mysql_version					="4.1.2"
addon_access_control			=0
author							="Cantico"
icon							="mail_message_new.png"
mysql_character_set_database	="latin1,utf8"
configuration_page				="admin"
mod_curl						="Available"
;*/?>