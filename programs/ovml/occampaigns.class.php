<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__) . '/../functions.php';

/**
 * The node attribute is a sitemap node id
 *
 * <OCMailingCampaigns [limit="[offset,]maxNumber"]>
 *      <OVId>
 *      <OVTitle>
 *      <OVDate>
 *      <OVPageUrl>
 * </OCMailingCampaigns>
 *
 */// @codingStandardsIgnoreStart
class Func_Ovml_Container_MailingCampaigns extends Func_Ovml_Container
{
    // @codingStandardsIgnoreEnd

    /**
     * @var Func_Mailing
     */
    private $ML;

    /**
     * @var array
     */
    private $campaigns;


    private $isStarred = null;


    /**
     * {@inheritDoc}
     * @see Func_Ovml_Container::setOvmlContext()
     */
    public function setOvmlContext(babOvTemplate $ctx)
    {
        parent::setOvmlContext($ctx);

        $this->ML = bab_functionality::get('Mailing');

        $this->limitOffset = 0;

        $limit = $ctx->getAttribute('limit');
        if (is_string($limit)) {
            $limits = explode(',', $limit);
            if (count($limits) === 1) {
                $this->limitRows = $limit;
            } else {
                $this->limitOffset = $limits[0];
                $this->limitRows = $limits[1];
            }
        }

        $this->isStarred = null;

        $starred = $ctx->getAttribute('starred');
        if (is_string($starred)) {
            if ($starred === '0') {
                $this->isStarred = false;
            } elseif ($starred === '1') {
                $this->isStarred = true;
            }
        }

        $this->idx += $this->limitOffset;

        $this->campaigns = $this->ML->getCampaignsInfo($this->isStarred);

        reset($this->campaigns);

        $this->count = count($this->campaigns);
        $this->ctx->curctx->push('CCount', $this->count);
    }


    /**
     * (non-PHPdoc)
     * @see utilit/Func_Ovml_Container#getnext()
     */
    public function getnext()
    {
        if ($this->idx >= $this->count || (isset($this->limitRows) && ($this->idx >= $this->limitRows + $this->limitOffset))) {
            $this->idx = $this->limitOffset;
            return false;
        }

        $this->ctx->curctx->push('CIndex', $this->idx);

        /* @var  $record prestameetin_FreeSlot */
        list(,$campaign) = each($this->campaigns);


        $this->ctx->curctx->push('Id', $campaign['id']);
        $this->ctx->curctx->push('Date', $campaign['date']);
        $this->ctx->curctx->push('Title', $campaign['title']);
        $this->ctx->curctx->push('PageUrl', $campaign['pageUrl']);


        $this->idx++;
        $this->index = $this->idx;
//        $this->records->next();
        return true;
    }
}
