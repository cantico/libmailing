<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';


class Func_Mailing extends bab_functionality
{
    public function getDescription()
    {
        return 'Mailing';
    }

    /**
     * call getStats method on the correct sub func mailing.
     *
     * @param int	$id		global ID
     *
     * @return array		stats, see getStats method.
     */
    public static function getGlobalStats($id)
    {
        require_once dirname(__FILE__).'/functions.php';
        require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';

        $set = LibMailing_GlobalSet();
        $global = $set->get($set->id->is($id));
        if (!$global) {
            return false;
        }
        $func_class = $global->api_class;
        $path = bab_functionalities::getPath($func_class);
        $func = bab_functionality::get($path);

        return $func->getStats($global->api_id);
    }

    /**
     * Send mail using the actual solution
     *
     * mandatory method
     *
     * @param LibMailingMailing		$mailing
     *
     * @return integer	containing the id of the sending.
     */
    public function send(LibMailingMailing $mailing)
    {
        throw new LibMailingException(__FUNCTION__.' Not implemented');
    }

    /**
     * Get mailing object
     *
     *
     * @return LibMailingMailing
     */
    public function getMailing()
    {
        return new LibMailingMailing();
    }

    /**
     * Get list of mailing send
     *
     * mandatory method
     *
     * @return array	id => array('title', 'send', 'block', 'bounce', 'open', 'click') where send, open, block are number
     */
    public function getMailingLists()
    {
        throw new LibMailingException(__FUNCTION__.' Not implemented');
    }

    /**
     * Get list of recipeient with status for a specific mailing send
     *
     * mandatory method
     *
     * @return array	id => array('email', 'status' => ('send', 'open', 'block')) where send, open, block are number
     */
    public function getStats($mailingId)
    {
        throw new LibMailingException(__FUNCTION__.' Not implemented');
    }

    /**
     * Get list of recipeient with status for a specific mailing send
     *
     * mandatory method
     *
     * @return array	id => array('email', 'status' => ('send', 'open', 'block')) where send, open, block are number
     */
    public function getContactStats($email)
    {
        throw new LibMailingException(__FUNCTION__.' Not implemented');
    }

    /**
     * Test if the functionality can be used
     * @return bool
     */
    public function isConfigured()
    {
         return false;
    }


    protected function embedImage($mailing)
    {
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/LibMailing/');
        $inline = $registry->getValue('inline_image', '1');

        $message = $mailing->content;
        $sizeMail = 0;
        $mailEmbeddedFiles = array();

        preg_match_all('/<img[^>]+>/i',$message, $resultImg);
        $imgs = array();
        $indexImage = 0;
        foreach($resultImg[0] as $img_tag)
        {
            preg_match_all('/(src)=("[^"]*")/i',$img_tag, $imgs[$indexImage]);
            $imgs[$indexImage]['tag'] = $img_tag;
            $indexImage++;
        }

        foreach($imgs as $img){
            $originalPath = str_replace('"', '', $img[2][0]);
            $nameArray = explode('/', $originalPath);
            $fileName = array_pop($nameArray);
            $cid = md5($originalPath);
            $mimetype = bab_getFileMimeType($originalPath);

            $addonInfos = bab_getAddonInfosInstance('publication');
            $babPath = new bab_Path($addonInfos->getUploadPath());
            $babPath->createDir();
            $handle = fopen($addonInfos->getUploadPath().$fileName, "w");
            if($handle){
                fwrite($handle, file_get_contents($originalPath));
                fclose($handle);
            }

            $fpath = $addonInfos->getUploadPath().$fileName;

            $styles = array();
            $w = 1200;
            $h = 1200;
            $returnStyle = preg_match_all('/style="([^"]*)"/i',$img['tag'], $styles);
            if($returnStyle && isset($styles[1]) && isset($styles[1][0])){
                $sizes = explode(';', $styles[1][0]);
                foreach($sizes as $size){
                    $temp = explode(':', $size);
                    foreach($temp as $k => $v){
                        $temp[$k] = strtolower(trim($v));
                    }
                    if(isset($temp[1]) && substr($temp[1], -2) == 'px'){
                        if($temp[0] == 'width'){
                            $w = str_replace('px', '', $temp[1]);
                        }elseif($temp[0] == 'height'){
                            $h = str_replace('px', '', $temp[1]);
                        }
                    }
                }
            }
            if ($T = @bab_functionality::get('Thumbnailer')){
                /*@var $T Func_Thumbnailer */
                $T->setSourceFile($fpath);
                $T->setResizeMode(Func_Thumbnailer::KEEP_ASPECT_RATIO);
                $urlImage = $T->getThumbnail($w, $h);
                $pathImage = $T->getThumbnailPath($w, $h);
            }

            if($inline === '1') {
                $mailEmbeddedFiles[$fileName] = array('path' => $pathImage, 'filename' => $fileName, 'mimtype' => $mimetype);
                $message = str_replace($originalPath, 'cid:'.$fileName, $message);
            } else {
                $message = str_replace($originalPath, $GLOBALS['babUrl'].$urlImage, $message);
            }
        }

        preg_match_all('/<a.*href="([^"]+)".*>(.*)<\/a>/isU',$message, $resultFile);
        $files = array();
        $indexFile = 0;
        foreach($resultFile[0] as $file_tag)
        {
        	$files[$indexFile] = array();
			$files[$indexFile]['tag'] = $file_tag;
			$files[$indexFile]['url'] = $resultFile[1][$indexFile];
			$files[$indexFile]['content'] = $resultFile[2][$indexFile];
        	$indexFile++;
        }
		
        foreach($files as $file){
        	$paramArray = array();
            $url = $file['url'];
			$cid = md5($url);
			if ($url) {
				$param = explode('?', $url);
				if (isset($param[1])) {
					$param[1] = str_replace('&amp;', '&', $param[1]);
					$params = explode('&', $param[1]);
					
					foreach ($params as $p) {
						$paramValue = explode("=", $p);
						$paramArray[$paramValue[0]] = str_replace('%2F', '/', $paramValue[1]);
					}
				}
			}
			if (isset($paramArray['uuid']) && isset($paramArray['idx']) && $paramArray['idx'] == 'download.uuid' && isset($paramArray['tg']) && $paramArray['tg'] == 'addon/publication/main') {
				$session = bab_getInstance('bab_Session');
		        if (isset($session->ofdownloadurl[$paramArray['uuid']])) {
		        	$filePath = new \bab_Path($session->ofdownloadurl[$paramArray['uuid']]);
					
					if ($filePath->isFile()) {
						$mime = bab_getFileMimeType($filePath->toString());
						//OTHER METHOD
						//$mailEmbeddedFiles[$cid] = array('path' => $filePath, 'filename' => $filePath->getBasename(), 'mimtype' => $mime);
						//$message = str_replace($url, 'cid:'.$cid, $message);
		        		
						//OTHER METHOD
						$mailing->addAttachement($filePath, $mime, \bab_Path::decode($filePath->getBasename()));
						$message = str_replace($file['tag'], \bab_Path::decode($filePath->getBasename()).' '.LibMailing_translate('(Join file)'), $message);
					}
		        } else {
		        	//throw new \bab_AccessException(translate('Access denied'));
		        }
			}
        }

        $sizeMail+= strlen($message);

        foreach($mailEmbeddedFiles as $cid => $file){
            $mailing->addAttachement($file['path'], $file['mimtype'], $file['filename'], $cid);
        }
		
		foreach ($mailing->attachements as $attachment) {//CHECK ALL ATTACHEMENTS
            $sizeMail+= filesize($attachment['path']);
		}
		
        $sizeMail = ($sizeMail / 1024)/1024;

        $mailing->content = $message;

        if(($sizeMail) > 14) {
            return false;
        }

        return true;
    }
}


/**
 *
 *
 */
class LibMailingException extends Exception {

}

class LibMailingMailing {
    public $title = '';
    public $content = '';
    public $content_alt = '';
    public $recipients = array();
    public $attachements = array();
    public $from_email = '';
    public $from_name = '';

    public function __construct()
    {
        $this->setFrom($GLOBALS['babAdminEmail'], $GLOBALS['babAdminName']);
    }

    public function setFrom($email, $name = '')
    {
        $this->from_email = $email;
        $this->from_name = $name;
    }

    /**
     * @param array $recipients  	each recipinet should be email & name
     */
    public function addRecipients($recipients)
    {
        foreach ($recipients as $recipient) {
            $this->addRecipient($recipient);
        }

        return $this;
    }

    /**
     * @param array $recipient  	recipinet should be email & name
     */
    public function addRecipient($recipient)
    {
        if (!is_array($recipient)) {
            $this->recipients[$recipient] = array(
                'email'=> $recipient,
                'name'=> ''
            );
        } else {
            if(isset($recipient['email'])) {
                $this->recipients[$recipient['email']] = array(
                    'email'=> $recipient['email'],
                    'name'=> isset($recipient['name'])?$recipient['name']:''
                );
            }
        }

        return $this;
    }

    /**
     * @todo
     */
    public function addMailingList($idlist)
    {
        $ML = bab_functionality::getOriginal('MailingList');
        if(!$ML) {
            throw new LibMailingException('MailingList functionality Not found');
        }
        $lists = $ML->getUsableLists();
        if (!isset($lists[$idlist])) {
            throw new LibMailingException('Access denied');
        }
        $recipients = $ML->getListSubscriptions($idlist);
        foreach ($recipients as $recipient) {
            $this->addRecipient(
                array(
                    'email' => $recipient->getEmail(),
                    'name' => $recipient->getName()
                )
            );
        }

        return $this;
    }

    public function addAttachement($path, $mimtype, $filename = '', $cid = '')
    {
        $this->attachements[] = array(
            'path' => $path,
            'cid' => $cid,
            'filename' => $filename,
            'mimtype' => $mimtype
        );
        return $this;
    }
}
