<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';

// We use oxygen if it is available.
if ($icons = @bab_functionality::get('Icons/Oxygen')) {
    $icons->includeCss();
} else if ($icons = @bab_functionality::get('Icons')) {
    $icons->includeCss();
}

class LibMailing_ConfigurationPage
{


    private function getForm()
    {
        $W = bab_Widgets();
        $form = $W->Form();
        $form->setName('configuration')->addClass('BabLoginMenuBackground')->addClass('LibMailing-form');
        $form->setHiddenValue('tg', bab_rp('tg'));
        $form->colon();

        $form->addClass(Func_Icons::ICON_LEFT_16)->getLayout()->setVerticalSpacing(1,'em');

        $options = array(
            '0' => LibMailing_translate('No'),
            '1' => LibMailing_translate('Yes')
        );

        $form->addItem(
            $W->LabelledWidget(
                LibMailing_translate('Put image into the mail'),
                $W->Select()->setOptions($options),
                'inline_image',
                LibMailing_translate('If your server is not accessible threw internet you have to say yes. If it is we encourage you to say no.')
            )
        );

        @$content = file_get_contents("https://www.google.fr/");
        if(!$content) {
            $infoInternet = LibMailing_translate('Internet seems to not be accessible.');
        } else {
            $infoInternet = LibMailing_translate('Internet seems to be accessible.');
        }
        $form->addItem(
            $W->Icon($infoInternet, Func_Icons::STATUS_DIALOG_INFORMATION)
        );

        /*$form->addItem($W->Title(LibMailing_translate('YMLP'),2));

        $label = $W->Label(LibMailing_translate('API key'));
        $input = $W->LineEdit()->setAssociatedLabel($label)->setSize(60)->setName('ymlp_api_key');

        $form->addItem($W->VBoxItems($label, $input));

        $label = $W->Label(LibMailing_translate('API user name'));
        $input = $W->LineEdit()->setAssociatedLabel($label)->setSize(60)->setName('ymlp_api_user_name');

        $form->addItem($W->VBoxItems($label, $input));*/

        $form->addItem($W->Title(LibMailing_translate('Mailjet'),2));

        $form->addItem(
            $W->LabelledWidget(
                LibMailing_translate('API key'),
                $W->LineEdit()->setSize(60),
                'mailjet_api_key'
            )
        );
        $form->addItem(
            $W->LabelledWidget(
                LibMailing_translate('API secret key'),
                $W->LineEdit()->setSize(60),
                'mailjet_api_secret_key'
            )
        );

        $form->addItem(
                $W->SubmitButton()
                ->setLabel(LibMailing_translate('Save'))
        );

        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/LibMailing/');


        $form->setValues(
            array(
                'configuration' => array(
                    'inline_image' 			=> $registry->getValue('inline_image'),
                        /*
                    'next_page'					=> $registry->getValue('next_page'),

                    'ymlp_api_key' 				=> $registry->getValue('ymlp_api_key'),
                    'ymlp_api_user_name' 		=> $registry->getValue('ymlp_api_user_name'),*/

                    'mailjet_api_key' 			=> $registry->getValue('mailjet_api_key'),
                    'mailjet_api_secret_key' 	=> $registry->getValue('mailjet_api_secret_key')
                )
            )
        );

        return $form;
    }




    public function display()
    {
        $W = bab_Widgets();
        $page = $W->BabPage();
        $page->addStyleSheet($GLOBALS['babInstallPath'].'styles/addons/LibMailing/main.css');


        $page->addItem($this->getForm());
        $page->displayHtml();
    }


    public function save($configuration)
    {
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/LibMailing/');

        $registry->setKeyValue('inline_image'		, $configuration['inline_image']);
        /*$registry->setKeyValue('next_page'				, $configuration['next_page']);
        $registry->setKeyValue('ymlp_api_key'			, $configuration['ymlp_api_key']);
        $registry->setKeyValue('ymlp_api_user_name'		, $configuration['ymlp_api_user_name']);*/
        $registry->setKeyValue('mailjet_api_key'		, $configuration['mailjet_api_key']);
        $registry->setKeyValue('mailjet_api_secret_key'	, $configuration['mailjet_api_secret_key']);
    }
}


if (!bab_isUserAdministrator())
{
    return;
}


$page = new LibMailing_ConfigurationPage;

if (!empty($_POST))
{
    $page->save(bab_pp('configuration'));
}

$page->display();