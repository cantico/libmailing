<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
bab_functionality::includeOriginal('Mailing');

class Func_Mailing_Mailjet extends Func_Mailing
{
    const customID = 'OviMailing_';

    const TYPE_TRANSACTIONAL = 1;
    const TYPE_CAMPAIGN = 2;

    private $api = null;

    public function getCustomID()
    {
        return self::customID;
    }

    public function getDescription()
    {
        return 'Mailjet http://www.mailjet.com';
    }


    /**
     * @return bool
     */
    public function isConfigured()
    {
        try {
            $this->API();
        } catch(LibMailingMailjetException $e) {
            return false;
        }

        return true;
    }


    private function includeFiles()
    {
        require_once dirname(__FILE__).'/autoload.php';
    }


    /**
     *
     * @throws LibMailingMailjetException
     * @return Mailjet
     */
    private function API()
    {
        //if (null === $this->api)
        {
            if (!class_exists('\\Mailjet\\Client')) {
                $this->includeFiles();
            }

            $registry = bab_getRegistryInstance();
            $registry->changeDirectory('/LibMailing/');

            $I = $registry->getValueEx(array('mailjet_api_key', 'mailjet_api_secret_key'));

            foreach($I as $arr)
            {
                ${$arr['key']} = $arr['value'];
            }

            if ((!isset($mailjet_api_key)) || (!isset($mailjet_api_secret_key)))
            {
                throw new LibMailingMailjetException('missing Mailjet configuration');
            }


            $this->api = new \Mailjet\Client($mailjet_api_key, $mailjet_api_secret_key);
            //$this->api->debug = 0;
        }

        return $this->api;
    }

    /**
     * Send mail using the actual solution
     *
     * mandatory method
     *
     * @param LibMailingMailing		$mailing
     *
     * @return integer	containing the id of the sending.
     */
    public function send($mailing)
    {
        require_once dirname(__FILE__).'/../../functions.php';
        require_once $GLOBALS['babInstallPath'].'utilit/uuid.php';

        $mailing->content_alt = strip_tags($mailing->content_alt);//TEXT ONLY
        if(!$this->embedImage($mailing)) {
            throw new LibMailingMailjetException('Mail is too big.');
        }


        $set = $this->getMailingSet();
        $mailingOrm = $set->newRecord();
        $mailingOrm->title = $mailing->title;
        $mailingOrm->content = $mailing->content;
        $mailingOrm->content_alt = $mailing->content_alt;
        $mailingOrm->save();

        $globalSet = LibMailing_GlobalSet();
        $global = $globalSet->newRecord();
        $global->api_id = $mailingOrm->id;
        $global->api_class = __CLASS__;
        $global->save();

        $api = $this->API();

        $recipientsParam = array();
        foreach ($mailing->recipients as $recipient) {
            $recipientsParam[] = array(
                'Email'=> $recipient['email'],
                'Name' => $recipient['name']
            );
        }

        $attachments = array();
        $inline_attachments = array();
        foreach ($mailing->attachements as $attachment) {
            if($attachment['cid']) {
                $inline_attachments[] = array(
                    'Content-type' => $attachment['mimtype'],
                    'Filename' => $attachment['cid'],
                    'content' => base64_encode(file_get_contents($attachment['path']))
                );
            } else {
                $attachments[] = array(
                    'Content-type' => $attachment['mimtype'],
                    'Filename' => $attachment['filename'],
                    'content' => base64_encode(file_get_contents($attachment['path']))
                );
            }
        }

        $body = [
            'FromEmail' => $mailing->from_email,
            'FromName' => $mailing->from_name,
            'Subject' => bab_getStringAccordingToDataBase($mailing->title, 'UTF-8'),
            //'Text-part' => bab_getStringAccordingToDataBase($mailing->content_alt, 'UTF-8'),
            'Html-part' => bab_getStringAccordingToDataBase($mailing->content, 'UTF-8'),
            'Recipients' => $recipientsParam,
            'Mj-campaign' => $this->getCustomID().$mailingOrm->id
        ];
        if ($mailing->content_alt) {
            $body['Text-part'] = bab_getStringAccordingToDataBase($mailing->content_alt, 'UTF-8');
        }

        if(!empty($attachments)) {
            $body['Attachments'] = $attachments;
        }
        if(!empty($inline_attachments)) {
            $body['Inline_attachments'] = $inline_attachments;
        }

        $response = $api->post(
            \Mailjet\Resources::$Email,
            [
                'body' => $body
            ]
        );

        if (!$response->success())
        {
            throw new LibMailingMailjetException($response->getStatus());
        }

        //return true;
        return $global->id;
    }

    /**
     *
     */
    public function getMailingSet()
    {
        require_once dirname(__FILE__).'/set/mailjetmailing.class.php';
        return new LibMailing_MailjetMailingSet();
    }

    protected function updateMailingID()
    {
        $api = $this->API();

        $set = $this->getMailingSet();
        $mailings = $set->select($set->mailjetID->is(''));
        foreach ($mailings as $mailing) {
            $response = $api->get(
                \Mailjet\Resources::$Campaign,
                [
                    'ID' => $this->getCustomID().$mailing->id
                ]
            );
            if (!$response->success())
            {
                //throw new LibMailingMailjetException($response->getStatus());
            } else {
                $datas = $response->getData();
                $mailing->mailjetID = $datas[0]['ID'];
                $mailing->save();
            }
        }

        return true;
    }

    /**
     * Get list of mailing send
     *
     * mandatory method
     *
     * @return array	id => array('title', 'date', 'send', 'block', 'bounce', 'open', 'click', 'spam') where send, open, block are number
     */
    public function getMailingLists()
    {
        $this->updateMailingID();
        $api = $this->API();

        $set = $this->getMailingSet();
        $mailings = $set->select($set->mailjetID->notIn(''));
        $customids = array();
        foreach ($mailings as $mailing) {
            $customids[$mailing->mailjetID] = $mailing->id;
        }

        //FromType' => '2' : Campaign
        //FromType' => '1' : Transactionnal
        $response = $api->get(
            \Mailjet\Resources::$Campaignstatistics,
            [
                'filters' =>
                [
                    'Limit' => '0',
                    'FromType' => self::TYPE_CAMPAIGN
                ]
            ]
        );

        if (!$response->success())
        {
            throw new LibMailingMailjetException($response->getStatus());
        }

        $return = array();
        $datas = $response->getData();

        foreach ($datas as $data) {
            if(isset($customids[$data['CampaignID']])) {
                $date = BAB_DateTime::fromICal(str_replace(array(':','-'), '', $data['CampaignSendStartAt']));
                $return[$customids[$data['CampaignID']]] = array(
                    'title' => $data['CampaignSubject'],
                    'date' => $date->getIsoDateTime(),
                    'send' => $data['ProcessedCount'],
                    'block' => $data['BlockedCount'],
                    'bounce' => $data['BouncedCount'],
                    'open' => $data['OpenedCount'],
                    'click' => $data['ClickedCount'],
                    'spam' => $data['SpamComplaintCount']
                );
            }
        }

        return $return;
    }






    /**
     * Returns information about campaigns.
     *
     * Direct access to remote-stored campaign information.
     *
     * @param bool      $isStarred
     * @throws LibMailingMailjetException
     * @return string[][] Array keys : 'id', 'date', 'title', 'pageUrl'
     */
    public function getCampaignsInfo($isStarred = null)
    {
        $api = $this->API();

        $drafts = array();
        $results = array();

        $campaignDraftResponse = $api->get(
            \Mailjet\Resources::$Campaigndraft, [
                'filters' => [
                    'Limit' => '0',
                ]
            ]
        );

        if (!$campaignDraftResponse->success()) {
            throw new LibMailingMailjetException($campaignDraftResponse->getStatus());
        }


        $campaignDrafts = $campaignDraftResponse->getData();

        foreach ($campaignDrafts as $campaignDraft) {
            $drafts[$campaignDraft['ID']] = $campaignDraft;
        }

        $filters = [
            'Limit' => '0', // No limit (in fact 1000 max)
            'FromType' => self::TYPE_CAMPAIGN,
            'Sort' => 'ID DESC',
        ];
        if (isset($isStarred)) {
            $filters['IsStarred'] = $isStarred;
        }
        $campaignResponse = $api->get(
            \Mailjet\Resources::$Campaign, [
                'filters' => $filters
            ]
        );

        if (!$campaignResponse->success()) {
            throw new LibMailingMailjetException($campaignResponse->getStatus());
        }


        $campaigns = $campaignResponse->getData();

        foreach ($campaigns as $campaign) {
            $id = bab_getStringAccordingToDataBase($campaign['ID'], bab_charset::UTF_8);
            $date = BAB_DateTime::fromICal(str_replace(array(':','-'), '', $campaign['SendStartAt']));
            $url = bab_getStringAccordingToDataBase($drafts[$campaign['NewsLetterID']]['Url'], bab_charset::UTF_8);

            $results[$id] = array(
                'id' => $id,
                'date' => $date->getIsoDateTime(),
                'title' => bab_getStringAccordingToDataBase($campaign['Subject'], bab_charset::UTF_8),
                'pageUrl' => $url,
            );
        }

        return $results;
    }



    /**
     * Get list of recipeient with status for a specific mailing send
     *
     * mandatory method
     *
     * @return array	id => array('email', 'status' => ('send', 'open', 'block')) where send, open, block are number
     */
    public function getStats($id)
    {
        $this->updateMailingID();
        $api = $this->API();

        $set = $this->getMailingSet();
        $mailing = $set->get($set->id->is($id));

        if (!$mailing) {
            return false;
        }

        $response = $api->get(
            \Mailjet\Resources::$Campaignstatistics,
            [
                'ID' => $mailing->mailjetID,
                'filters' =>
                [
                    'Limit' => '0',
                    'FromType' => self::TYPE_CAMPAIGN,
                ]
            ]
        );

        if (!$response->success())
        {
            throw new LibMailingMailjetException($response->getStatus());
        }

        $return = array();
        $datas = $response->getData();

        foreach ($datas as $data) {
            $date = BAB_DateTime::fromICal(str_replace(array(':','-'), '', $data['CampaignSendStartAt']));
            $return[$mailing->id] = array(
                /*'id' => $mailing->id,*/
                'title' => $data['CampaignSubject'],
                'date' => $date->getIsoDateTime(),
                'send' => $data['ProcessedCount'],
                'block' => $data['BlockedCount'],
                'bounce' => $data['BouncedCount'],
                'open' => $data['OpenedCount'],
                'click' => $data['ClickedCount'],
                'spam' => $data['SpamComplaintCount']
            );
        }

        return $return;
    }

    /**
     * Get list of recipeient with status for a specific mailing send
     *
     * mandatory method
     *
     * @return array	id => array('email', 'status' => ('send', 'open', 'block')) where send, open, block are number
     */
    public function getContactStats($email)
    {

    }



    /**
     *
     * @return array()
     */
    public function getLists()
    {
        $api = $this->API();
        $response = $api->get(\Mailjet\Resources::$Contactslist);

        if (!$response->success())
        {
            return array();
        }

        $return = array();
        foreach($response->getData() as $list)
        {
            $return[$list['ID']] = bab_getStringAccordingToDataBase($list['Name'], 'UTF-8');
        }

        return $return;
    }



    /**
     * Create a new contact
     *
     * mandatory method
     *
     * @param	string	$email
     * @param	string	$name
     * @param	Array	$lists		list of id
     *
     * @return bool
     */
    public function addContact($email, $name, Array $lists)
    {
        if (empty($lists))
        {
            throw new LibMailingMailjetException('For mailjet at least one mailling list ID must be specified to create a contact');
        }

        $status = true;
        foreach($lists as $id_list)
        {
            try {
                $this->subscribeContact($email, $name, $id_list);
            } catch(LibMailingException $e)
            {
                $status = false;
            }
        }

        return $status;
    }



    /**
     * Get the list where the contact is subscribed
     * return an array where keys are mailling list id, values are malling lists names (label visible by user)
     * return null if the contact does not exists
     * @return array
     */
    public function getContactSubscriptions($email)
    {
        $api = $this->API();
        $response = $api->get(
            \Mailjet\Resources::$ContactGetcontactslists,
            [
                'id' => bab_convertStringFromDatabase($email, 'UTF-8')
            ]
        );

        if (!$response->success())
        {
            return null;
        }

        $return = array();

        foreach($response->getData() as $l)
        {
            if (!$l['IsUnsub'] && $l['IsActive'])
            {
                $listInfo = $this->getListInfo($l['ListID']);
                $return[$l['ListID']] = bab_getStringAccordingToDataBase($listInfo['Name'], 'UTF-8');
            }
        }

        return $return;
    }


    public function getListInfo($list)
    {
        $api = $this->API();
        $response = $api->get(
            \Mailjet\Resources::$Contactslist,
            [
                'id' => $list
            ]
        );

        if (!$response->success())
        {
            return array();
        }

        return $response->getData()[0];
    }




    /**
     * Subscribe contact to a list
     * Return true if contact subscribed
     * if the contact does not exists, il will be created
     *
     * @throws LibMailingException throws an exception if the contact cannot be subscribed
     *
     * @param	string	$email
     * @param	string	$name
     * @param	string	$list		list ID
     */
    public function subscribeContact($email, $name, $list)
    {
        $api = $this->API();

        $body = [
            'Email' => bab_getStringAccordingToDataBase($email, 'UTF-8'),
            'Name' => bab_getStringAccordingToDataBase($name, 'UTF-8'),
            'Action' => 'addnoforce'
        ];

        $response = $api->post(
            \Mailjet\Resources::$ContactslistManagecontact,
            [
                'id' => $list,
                'body' => $body
            ]
        );

        if (!$response->success())
        {
            throw new LibMailingMailjetException($response->getStatus());
        }

        return true;
    }





    /**
     * Unsubscribe contact from a list
     * Return true if contact unsuscribed
     *
     * @throws LibMailingException throws an exception if the contact cannot be unsubscribed
     *
     * @param	string	$email
     * @param	string	$list		list ID
     */
    public function unsubscribeContact($email, $list)
    {

        $api = $this->API();

        $body = [
            'Action' => 'unsub',
            'Contacts' => [
                ['Email' => bab_getStringAccordingToDataBase($email, 'UTF-8')]
            ]
        ];

        $response = $api->post(
            \Mailjet\Resources::$ContactslistManagecontact,
            [
                'id' => $list,
                'body' => $body
            ]
        );

        if (!$response->success())
        {
            throw new LibMailingMailjetException($response->getStatus());
        }

        return true;
    }




    /**
     * Delete contact
     * return true if contact removed
     *
     * @param	string $email
     * @param	Array	$list		id_list as values, if not set, the contact will be removed from all the lists
     *
     * @return bool
     */
    public function removeContact($email, Array $lists = null)
    {
        $api = $this->API();

        if (null === $lists)
        {
            $lists = array_keys($this->getLists());
        }

        $body = [
            'Action' => 'remove',
            'Contacts' => [
                ['Email' => bab_getStringAccordingToDataBase($email, 'UTF-8')]
            ]
        ];


        foreach($lists as $id_list)
        {
            $response = $api->post(
                \Mailjet\Resources::$ContactslistManagecontact,
                [
                    'id' => $id_list,
                    'body' => $body
                ]
            );
        }
    }
	
	/**
	 * Add a callback to mailjet
	 * 
	 * @param string $eventType
	 * One of this:
	 * open
	 * click
	 * bounce
	 * spam
	 * blocked
	 * unsub
	 * typofix
	 * sent
	 * parseapi
	 * newsender
	 * newsenderautovalid
	 * 
	 * @param string $url	Should contain a id token to not authorise call from outside, and should handle treatment unsynchronously.
	 * 						Must also return 200 OK HTTP code to validate treatment, if not, the callback will be redone.
	 * 						You can also have only one call back url by event type.
	 * 
	 * @return bool
	 * 
	 */
	public function addCallback($eventType, $url)
	{
        $api = $this->API();
		
		$body = [
            'EventType' => $eventType,
            'Url' => $url,
            'Version' => '2',
            'IsBackup' => false
        ];
		
		$response = $api->post(
            \Mailjet\Resources::$Eventcallbackurl,
            [
                'body' => $body
            ]
        );
		
		if (!$response->success())
        {
            throw new LibMailingMailjetException($response->getStatus());
        }

        return true;
	}
	
	/**
	 * remove callback url of a specific type
	 */
	public function removeCallback($eventType)
	{
        $api = $this->API();
		
		$body = [
            'EventType' => $eventType,
            'IsBackup' => false
        ];
		
		$response = $api->delete(
            \Mailjet\Resources::$Eventcallbackurl,
            [
                'body' => $body
            ]
        );
		
		if (!$response->success())
        {
            throw new LibMailingMailjetException($response->getStatus());
        }

        return true;
	}
}



class LibMailingMailjetException extends LibMailingException {
}

