<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';






function LibMailing_upgrade($sVersionBase, $sVersionIni)
{
	require_once dirname(__FILE__).'/functions.php';
	require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
	require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';

	$functionalities = new bab_functionalities();

	$addon = bab_getAddonInfosInstance('LibMailing');

	$addonPhpPath = $addon->getPhpPath();

	$functionalities->registerClass('Func_Mailing', $addonPhpPath. '/mailing.class.php');
	//$functionalities->registerClass('Func_Mailing_Ovidentia', $addon->getPhpPath() . '/api/ovidentia/ovidentia.class.php');
	//$functionalities->registerClass('Func_Mailing_Ymlp', $addon->getPhpPath() . '/api/ymlp/ymlp.class.php');
	$functionalities->unregister('Mailing/Ymlp');
	$functionalities->registerClass('Func_Mailing_Mailjet', $addonPhpPath. '/api/mailjet/mailjet.class.php');


	$synchronize = new bab_synchronizeSql();
	$synchronize->addOrmSet(LibMailing_GlobalSet());
	$M = bab_functionality::get('Mailing/Mailjet');
    $synchronize->addOrmSet($M->getMailingSet());
    $synchronize->updateDatabase();

    $functionalities->registerClass('Func_Ovml_Container_MailingCampaigns', $addonPhpPath . 'ovml/occampaigns.class.php');

	return true;
}


function LibMailing_onDeleteAddon()
{
	require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
	$functionalities = new bab_functionalities();
	$functionalities->unregister('Ovml/Container/MailingCampaigns');
	//$functionalities->unregister('Mailing/Ovidentia');
	$functionalities->unregister('Mailing/Mailjet');
	$functionalities->unregister('Mailing/Ymlp');
	$functionalities->unregister('Mailing');
	return true;
}
