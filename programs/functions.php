<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';


function LibMailing_translate($str, $str_plurals = null, $number = null)
{
    if ($translate = bab_functionality::get('Translate/Gettext'))
    {
        /* @var $translate Func_Translate_Gettext */
        $translate->setAddonName('LibMailing');

        return $translate->translate($str, $str_plurals, $number);
    }

    return $str;
}


/**
 * Initialize mysql ORM backend.
 */
function LibMailing_loadOrm()
{
    static $loadOrmDone = false;

    if (!$loadOrmDone) {

        $Orm = bab_functionality::get('LibOrm');
        /*@var $Orm Func_LibOrm */
        $Orm->initMySql();

        $mysqlbackend = new ORM_MySqlBackend($GLOBALS['babDB']);
        ORM_MySqlRecordSet::setBackend($mysqlbackend);

        $loadOrmDone = true;
    }
}



function LibMailing_IncludeSet()
{
    require_once dirname(__FILE__).'/set/mailing.class.php';
	require_once dirname(__FILE__).'/set/global.class.php';
}

/**
 * @return LibMailing_MailingSet
 */
function LibMailing_MailingSet()
{
    LibMailing_IncludeSet();

    return new LibMailing_MailingSet();
}

/**
 * @return LibMailing_GlobalSet
 */
function LibMailing_GlobalSet()
{
    LibMailing_IncludeSet();

    return new LibMailing_GlobalSet();
}